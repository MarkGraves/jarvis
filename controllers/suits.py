# -*- coding: utf-8 -*-

table = db.suit

def index():
    grid = SQLFORM.grid(table)
    return dict(grid=grid)

def create_suit():
    form = SQLFORM(table)
    if form.process().accepted:
        session.flash = "Connecting to the Cisco for reconfiguration of shell metals"
        return redirect(URL('index'))
    return dict(form=form)

def delete_suit():
    query = table.id == request.args(0)
    db(query).delete()
    return redirect(URL('index'))

def update_name():
    query = table.id == request.args(0)
    row = db(query).select().first()
    fields = ['id','name']
    form = SQLFORM(table,row,fields=fields)
    if form.process().accepted:
        session.flash = "suit name updated"
        return redirect(URL('index'))
    else:
        return dict(form=form)
    
def update_color():
    query = table.id == request.args(0)
    row = db(query).select().first()
    fields = ['id','color']
    form = SQLFORM(table,row,fields=fields)
    if form.process().accepted:
        session.flash = "suit name updated"
        return redirect(URL('index'))
    else:
        return dict(form=form)
    
    
def update_last_used():
    query = table.id == request.args(0)
    row = db(query).select().first()
    fields = ['id','last_used']
    form = SQLFORM(table,row,fields=fields)
    if form.process().accepted:
        session.flash = "suit last used updated"
        return redirect(URL('index'))
    else:
        return dict(form=form)

    
def add_part():
    suit_id = request.args(0)
    part_id = request.args(1)
    
    row = table(suit_id)
    row.parts.append(part_id)
    
    row.parts = list(set(row.parts))
    
    row.update_record()
    
    session.flash = "Added Part"
    return redirect(URL('index'))
    
def remove_part():
    suit_id = request.args(0)
    part_id = request.args(1)
    
    row = table(suit_id)
                    
    row.parts = list(set([str(part) for part in row.parts if str(part) != part_id]))
    
    row.update_record()
    
    session.flash = "Removed Part"
    return redirect(URL('index'))
