# -*- coding: utf-8 -*-
table = db.part

def index():
    query = table.id >0    
    rows = db(query).select()
    return dict(rows=rows)


def create_part():
    form = SQLFORM(table)
    if form.process().accepted:
        session.flash = "Connecting to the Cisco for reconfiguration of shell metals"
        return redirect(URL('index'))
    return dict(form=form)


def update_name():
    query = table.id == request.args(0)
    row = db(query).select().first()
    fields = ['id','name']
    form = SQLFORM(table,row,fields=fields)
    if form.process().accepted:
        session.flash = "part name updated"
        return redirect(URL('index'))
    else:
        return dict(form=form)
    
def update_bar_code():
    query = table.id == request.args(0)
    row = db(query).select().first()
    fields = ['id','bar_code']
    form = SQLFORM(table,row,fields=fields)
    if form.process().accepted:
        session.flash = "part bar code updated"
        return redirect(URL('index'))
    else:
        return dict(form=form)
    
def delete_part():
    query = table.id == request.args(0)
    db(query).delete()
    return redirect(URL('index'))
