## JARVIS API
    
    /part[part]
    
    /part/id/{part.id}
    
    /part/id/{part.id}/:field
    
    /suit[suit]
    
    /suit/id/{suit.id}
    
    /suit/id/{suit.id}/:field
    
    /suit/last-used/{suit.last_used.year}
    
    /suit/last-used/{suit.last_used.year}/:field
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}/:field
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}/{suit.last_used.day}
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}/{suit.last_used.day}/:field
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}/{suit.last_used.day}/{suit.last_used.hour}
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}/{suit.last_used.day}/{suit.last_used.hour}/:field
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}/{suit.last_used.day}/{suit.last_used.hour}/{suit.last_used.minute}
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}/{suit.last_used.day}/{suit.last_used.hour}/{suit.last_used.minute}/:field
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}/{suit.last_used.day}/{suit.last_used.hour}/{suit.last_used.minute}/{suit.last_used.second}
    
    /suit/last-used/{suit.last_used.year}/{suit.last_used.month}/{suit.last_used.day}/{suit.last_used.hour}/{suit.last_used.minute}/{suit.last_used.second}/:field
    
    /suit/parts/{suit.parts.contains
    
    /suit/parts/{suit.parts.contains}/:field